package com.sbwithjsp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WelcomeController {

	@GetMapping("/hello")
	public String welcome() {
		return "welcome";
	}
}
